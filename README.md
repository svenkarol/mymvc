# Requirements-Management using MVC
This application can manage stakeholder and stakeholder requirements using a simple model-view-controller approach. There are three controllers. *requirement* manages stakeholder requirements, *stakeholder* manages the stakeholders and *stakeholder-requirement* manages the relation between requirements and stakeholders.

## How to run it
- install Node.js
- open a terminal
- clone respository: ````git clone git@bitbucket.org:svenkarol/mymvc.git````
- go to the *mymvc* directory and type ````npm install````
- after successful installation, type ````node .````
- open a browser and open ````http://localhost:3000````

You should now see the list of stakeholders.

**Note**: there is no real persistence layer. Data is only stored per session and then discarded.

## License
The license is MIT.