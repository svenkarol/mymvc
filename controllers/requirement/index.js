var model = require('../../model');

exports.engine = 'ejs';

exports.before = function (req, res, next) {
  var id = req.params.requirement_id;
  if (!id)
    return next('route');
  requirement = model.by_id(model.requirements, id);
  req.requirement = requirement;
  next();
};

exports.show = function (req, res, next) {
  res.render('show', {
    requirement: req.requirement,
    stakeholders: model.stakeholders_from_requirement(requirement)
  });
};

exports.edit = function (req, res, next) {
  res.render('edit', { requirement: req.requirement });
};

exports.update = function (req, res, next) {
  var body = req.body;
  req.requirement.name = body.requirement.name;
  req.requirement.description = body.requirement.description;
  res.message('Information updated!');
  res.redirect('/requirement/' + req.requirement.id);
};
