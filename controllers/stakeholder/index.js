var model = require('../../model');

exports.engine = 'hbs';

// Read the data from the fake data base by searching
// for the element with the corresponding ID.
exports.before = function (req, res, next) {
  var id = req.params.stakeholder_id;

  //no id provided, goto next handler
  if (!id)
    return next();

  process.nextTick(function () {
    req.stakeholder = model.by_id(model.stakeholders, id);
    if (!req.stakeholder)
      return next('route');
    next();
  });
};

exports.list = function (req, res, next) {
  res.render('list', { stakeholders: model.stakeholders });
};

exports.show = function (req, res, next) {
  res.render('show', {
    stakeholder: req.stakeholder
  });
};

exports.edit = function(req, res, next){
  res.render('edit', { stakeholder: req.stakeholder });
};

exports.update = function(req, res, next){
  var body = req.body;
  req.stakeholder.name = body.stakeholder.name;
  res.message('Information updated!');
  res.redirect('/stakeholder/' + req.stakeholder.id);
};

exports.create = function(req, res, next){
  var new_id = model.create_stakeholder()
  if(new_id)
  {
    res.message('Stakeholder created!');
    res.redirect('/stakeholder/' + new_id + '/edit');
    return;
  }
  res.message('Failed to create new stakeholder!');
  res.redirect('/stakeholders');
};
