/**
 * Module dependencies.
 */

var model = require('../../model');

exports.name = 'requirement';
exports.prefix = '/stakeholder/:stakeholder_id';

exports.create = function(req, res, next){
  var stake_id = req.params.stakeholder_id;
  var stakeholder = model.by_id(model.stakeholders, stake_id);
  var body = req.body;
  
  if (!stakeholder) 
      return next('route');
    
  var name = body.requirement.name;
  var req_id = body.requirement.id;
  if(name)
      model.add_requirement(name, stakeholder.id);
  if(req_id)
      model.create_link(stakeholder.id, req_id);

  res.message('Added requirement ');
  res.redirect('/stakeholder/' + stakeholder.id);
};
