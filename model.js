var hbs = require('hbs');

var stakeholders = exports.stakeholders = [];

stakeholders.push({ name: 'Driver', description: 'The driver of the Fantasy car.', id: 1000 })
stakeholders.push({ name: 'Owner', description: 'The owner/buyer of the Fantasy car.', id: 1001 })
stakeholders.push({ name: 'Mechanic', description: 'Mechanical engineer who maintains the car.', id: 1002 })
stakeholders.push({ name: 'Upper management', description: 'The management of our business.', id: 1003 })
stakeholders.push({ name: 'Legal department', description: 'The legal department of Fantasy Automotive.', id: 1004 })

var requirements = exports.requirements = [];
requirements.push({ name: 'Operability at highway speeds.', description: 'The system shall be operable at normal highway speeds.', id: 2001 })
requirements.push({ name: 'Conformance to ISO 26262.', description: 'The driving function shall conform to the safety rules in ISO 26262.', id: 2002 })

var stake2req = exports.stake2req = [];
stake2req.push({ stake_id: 1000, req_id: 2001 })

// DB Queries/Helpers
exports.by_id = function (list, id) {
    return list.find(entry => entry.id == id);
}

exports.add_requirement = function (req_name, stakeholder_id)
{
    new_id = Math.max.apply(Math,requirements.map(entry => entry.id)) + 1;
    requirements.push({ name: req_name, description: 'n/a', id: new_id });
    stake2req.push({ stake_id: stakeholder_id, req_id: new_id })
}

exports.create_stakeholder = function ()
{
    new_id = Math.max.apply(Math,stakeholders.map(entry => entry.id)) + 1;
    stakeholders.push({ name: '<<please define>>', description: 'n/a.', id: new_id })
    return new_id;
}

exports.stakeholders_from_requirement = function (requirement) {
    var stake_ids =
        stake2req
            .filter(entry => requirement.id == entry.req_id)
            .map(pair => pair.stake_id);
    return stakeholders.filter(entry => stake_ids.includes(entry.id));
}

exports.create_link = function (stake_id, req_id)
{
    var entry = { stake_id: Number(stake_id), req_id: Number(req_id) }
    if(!stake2req.includes(entry)){
        stake2req.push(entry)
    }
}

hbs.registerHelper('reqs', function (stakeholder) {
    var req_ids =
        stake2req
            .filter(entry => stakeholder.id == entry.stake_id)
            .map(pair => pair.req_id);
    return requirements.filter(entry => req_ids.includes(entry.id));
});

hbs.registerHelper('all_linkable_reqs', function (stakeholder) {
    var req_ids =
        stake2req
            .filter(entry => stakeholder.id == entry.stake_id)
            .map(pair => pair.req_id);
    return requirements.filter(entry => !req_ids.includes(entry.id));
});

hbs.registerHelper('all_reqs', function () {return requirements;});